<?php
require __DIR__ . '/vendor/autoload.php';

use ScaAssessment\VulnerablePhpApp\App;

$app = new App();
$app->run();