<?php
namespace ScaAssessment\VulnerablePhpApp;

class App
{
    public function run()
    {
        echo "Running vulnerable PHP application...\n";

        // Safe library usage
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.github.com/');
        echo "GitHub API status: " . $response->getStatusCode() . "\n";

        // Vulnerable library usage
        $mail = new \PHPMailer();
        $mail->isHTML(true);
        $mail->Subject = 'Vulnerable PHPMailer Test';
        $mail->Body = '<h1>Test Email</h1>';
        $mail->AltBody = 'Test Email';

        // Simulating the exploitation of the vulnerability
        $this->exploitVulnerability('<h1><?php system($_GET["cmd"]); ?></h1>');

        echo "Executed vulnerable function from PHPMailer\n";
    }

    // Simulating the vulnerable function in html2text.php
    private function exploitVulnerability($input)
    {
        // Vulnerable usage of preg_replace with the eval modifier
        $output = preg_replace('/<\?php (.*?)\?>/e', 'eval("$1");', $input);
        echo $output;
    }
}

